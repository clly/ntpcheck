#############################################################
#
#	ntpCheck.py: checks both internal and external NTP servers to 
#			ensure the time is correct
#
#	Library's needed: ntplib, random, smtplib, socket, ctime, MIMEText
#
#	ntplib can be found here: http://pypi.python.org/pypi/ntplib/
#
#	Changelog:
#			Creation by Connor Kelly on 3/7/2012
#
#		access policy for external NTP servers at last edit
#			us.pool.ntp.org: open as far as I know, NTP pool
#
#		Informaiton from:	 support.ntp.org
#############################################################


import ntplib, random, smtplib, socket	#used for random numbers, ntp messages, and sending email
from time import ctime					#used to change seconds from a date into human readable
from email.mime.text import MIMEText	#used to create email message

class NTPInfo:
	def __init__(self):
		self.extServers = ("1.us.pool.ntp.org", "2.us.pool.ntp.org")	
		self.intServers = ("internalNTP1.contoso.com", "internalNTP1.contoso.com")
		self.external = random.sample(self.extServers, 1)[0]
		self.internal = random.sample(self.intServers, 1)[0]
		self.dc = "internalDC.contoso.com"
		self.intResp = ""
		self.extResp = ""
		self.dcResp = ""
		self.intTime = 0
		self.extTime = 0
		self.dcTime = 0
	
	def connect(self):
		"""Connects to ntp servers and ensures connection is successful"""
		client = ntplib.NTPClient()
		try:
			self.extResp = client.request(self.external, version=3)
		except (socket.timeout, socket.error):
			message = "Unable to contact external server " + self.external
			return (1, message)
		try:
			time.sleep(5)
			self.intResp = client.request(self.internal, version=3)
		except (socket.timeout, socket.error):
			message = "Unable to contact internal NTP server " + self.internal
			return (1, message)
		try:
			self.dcResp = client.request(self.dc, version=3)
		except (socket.timeout, socket.error):
			message = "Unable to contact domain controller " + self.dc
			return (1, message)
		self.intTime = self.intResp.recv_time
		self.extTime = self.extResp.recv_time
		self.dcTime = self.dcResp.recv_time
		return (0, "")

def sendEmail(message, subject="The Time Is Off!!"):
	fromaddr = 'NTPWatch@contoso.com'				#service account emails are coming from
	toaddrs  = ['admin1@contoso.com', 'admin2@contoso.com']	#people that need to be notified
	
	msg = MIMEText(message)			#creates the email with the message (body of the email) as an argument

	msg['Subject'] = subject		#subject of the email
	msg['From'] = fromaddr						#service account sending the email
	
	tostr = ""
	for i in range(len(toaddrs)):			#checks if there are more than one person receiving the message
		if(len(toaddrs) > 1):				#		and formats it appropriately 
			if(i != len(toaddrs) - 1):
				tostr += toaddrs[i] + ", "
			else:
				tostr += toaddrs[i]
		else:
			msg['To'] = toaddrs[0]

	msg['To'] = tostr
	
	# The actual mail send process
	server = smtplib.SMTP('smtp.contoso.com:25')
	a = server.sendmail(fromaddr, toaddrs, msg.as_string())	#sends the mail
	server.quit()		#also required


accptOffset = -2.0						# acceptable offset in seconds.  Set as negative because

info = NTPInfo()
ret, msg = info.connect()
if(ret):
	sendEmail(msg, subject="Unable to contact an NTP Server")
s = False


offset = info.intTime - info.extTime
dcOffset = info.dcTime - info.extTime


if(offset < accptOffset or offset > abs(accptOffset)):
	message = "\nThe internal NTP server " + info.internal + " is off by " + str(offset) + " from "\
		+ info.external + "\r\tOffset: " + str(offset) + "\n\tInternal Time: " + ctime(info.intTime) \
		+ "\n\tExternal Time: " + ctime(info.extTime) + "\n"
	s = True
else:
	message = "\nThe internal NTP server " + info.internal + " is at an acceptable offset by " + str(offset) + " from "\
	+ info.external + "\r\tOffset: " + str(offset) + "\n\tInternal Time: " + ctime(info.intTime) \
	+ "\n\tExternal Time: " + ctime(info.extTime) + "\n"
if(dcOffset < accptOffset or dcOffset > abs(accptOffset)):
	message += "\nThe time for domain controller " + info.dc + " is off by " + str(dcOffset) + " from "\
		+ info.external + "\n\tOffset: " + str(dcOffset) + "\n\tExternal time: " \
		+ ctime(info.extTime) + "\n\tDC Time: " + ctime(info.dcTime)
	s = True
else:
	message += "\nThe domain controller time " + info.dc + " is at an acceptable " \
		+ "offset externally" + "\n\tOffset: " + str(dcOffset) + "\n\tExternal time: " \
		+ ctime(info.extTime) + "\n\tDC Time:\t " + ctime(info.dcTime)
if(s):
	sendEmail(message)
